## webhelpdesk

An image for running SolarWinds webhelpdesk under Kubernetes.

This does not start the embedded database, so you'll need to setup a database
to connect to. (It could be modified to use the embedded database).

Use the following environment variables to setup access to the database ...
they will be used to create an appropriate .whd.properties file.

    - WHD_DB_PORT
    - WHD_DB_SID
    - WHD_DB_SUBPROTOCOL
    - WHD_DB_CUSTOM_URL
    - WHD_DB_PASSWORD
    - WHD_DB_HOST
    - WHD_DB_USE_CUSTOM_URL
    - WHD_DB_NAME
    - WHD_DB_USERNAME
    - WHD_DB_EMBEDDED

Configuring database access from within won't work permanently, because changes
to .whd.properties won't be persisted. I suppose you could do it and then see
what the properties should be and change how you launch the container.

The container has an empty whd.conf. What the startup script does is "source"
whd.conf. So, for Kubernetes, just set the things you want in whd.conf as
environment variables. The Dockerfile will setup defaults according to
whd.conf.orig.

Upgrading webhelpdesk is a bit tricky in this environment. In addition to the
obvious steps, once you start up the new version, it will wait for a connection
from a privileged network to complete the database update. Because I've normally
got this behind an ingress, connecting directly isn't straightforward. There's
a command in deploy/Makefile to do port-forwarding -- this makes the connection
appear to come from localhost to webhelpdesk, which it always considers a
privileged network. But the port-forward is only available to localhost (where
you are running kubectl). If you have a browser available there, that could be
fine. If not, you can do a second layer of forwarding via something like:

    socat TCP-LISTEN:8082,fork TCP:localhost:8081

... and then contact port 8082 from a browser elsewhere.

Once the database schema is updated, webhelpdesk will try to restart, but that
doesn't seem to work quite right. You'll need to manually restart it:

    kubectl rollout restart statefulset/whd-frontend

(or something like that), and then things should be fine.
