#!/bin/bash

cd /usr/local/webhelpdesk

envsubst <conf/whd.properties.subst >conf/.whd.properties

# exec so that we replace ourselves with the startup script
# debug so that whd starts catalina in foreground
exec /usr/local/webhelpdesk/bin/whd debug
