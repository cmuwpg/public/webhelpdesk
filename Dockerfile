FROM almalinux:8

ENV WHD_VERSION=12.8.3 \
    WHD_BUILD=1813 \
    EMBEDDED=false

# These are from whd.conf.orig.
ENV DEFAULT_PORT=8081 \
    NO_REDIRECT=true \
    KEYSTORE_PASSWORD=changeit \
    KEYSTORE_TYPE=JKS \
    MINIMUM_MEMORY=256 \
    MAXIMUM_MEMORY=3072 \
    MAXIMUM_PERM_MEMORY=256 \
    DATABASE_CONNECTIONS=25 \
    TOMCAT_SERVER_PORT=23010

# getext is for envsubst, and iproute for ss, which whd uses
RUN yum install -y patch gettext iproute && yum clean all

# Get webhelpdesk
RUN curl -o /webhelpdesk.rpm.gz  https://downloads.solarwinds.com/solarwinds/Release/WebHelpDesk/$WHD_VERSION/webhelpdesk-$WHD_VERSION.$WHD_BUILD.x86_64.rpm.gz \
    && gunzip /webhelpdesk.rpm.gz \
    && yum install -y /webhelpdesk.rpm \
    && rm /webhelpdesk.rpm \
    && yum clean all

WORKDIR /usr/local/webhelpdesk

RUN yum install -y unzip && yum clean all

RUN curl -o WHD-12.8.3-HF2.zip https://downloads.solarwinds.com/solarwinds/Release/HotFix/WHD-12.8.3-HF2.zip \
    && unzip WHD-12.8.3-HF2.zip \
    && mv 12.8.3-HF2/bin/tomcat/lib/whd-security.jar bin/tomcat/lib/whd-security.jar \
    && mv 12.8.3-HF2/bin/webapps/helpdesk/WEB-INF/lib/whd-web.jar bin/webapps/helpdesk/WEB-INF/lib/whd-web.jar \
    && mv 12.8.3-HF2/bin/webapps/helpdesk/WEB-INF/lib/whd-core.jar bin/webapps/helpdesk/WEB-INF/lib/whd-core.jar \
    && rm WHD-12.8.3-HF2.zip \
    && rm -rf WHD-12.8.3-HF2

# Don't start the embedded database
COPY whd.diff bin/whd.diff
RUN patch bin/whd bin/whd.diff

COPY tomcat_server_template.xml.diff conf/tomcat_server_template.xml.diff
RUN patch conf/tomcat_server_template.xml conf/tomcat_server_template.xml.diff

# Don't use whd.config -- we'll supply the environment vars from Kubernetes,
# with the defaults given above.
RUN touch conf/whd.conf

# Construct a .whd.properties
COPY whd.properties.subst conf/whd.properties.subst
COPY run.sh run.sh

RUN echo " whd.conf\r--------\r" && cat conf/whd.conf.orig
RUN echo " \n\nREADME.txt\n----------\n" && cat README.txt

ENTRYPOINT ["/usr/local/webhelpdesk/run.sh"]
