apiVersion: v1
kind: ServiceAccount
metadata:
  name: awx-whd-reloader
  namespace: whd

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: whd
  name: awx-whd-reloader
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["delete","get"]
  resourceNames: ["whd-frontend-0"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  namespace: whd
  name: awx-whd-reloader
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: awx-whd-reloader
subjects:
- kind: ServiceAccount
  name: awx-whd-reloader
  namespace: whd

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: whd-system-prefs
  namespace: whd
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  storageClassName: csi-cephfs-triple

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: whd
  namespace: whd
data:
  WHD_DB_PORT: "5432"
  WHD_DB_SID: ""
  WHD_DB_SUBPROTOCOL: postgresql
  WHD_DB_CUSTOM_URL: ""
  WHD_DB_USE_CUSTOM_URL: "NO"
  WHD_DB_HOST: ops-pgbouncer.percona-pgsql.svc
  WHD_DB_NAME: whd
  WHD_DB_USERNAME: whd
  WHD_DB_EMBEDDED: "NO"

  DEFAULT_PORT: "8081"
  HTTPS_PORT: "8443"
  URL_HTTPS_PORT: "443"
  URL_DEFAULT_PORT: "80"
  NO_REDIRECT: "true"
  MINIMUM_MEMORY: "512"
  MAXIMUM_MEMORY: "4096"
  MAXIMUM_PERM_MEMORY: "256"
  DATABASE_CONNECTIONS: "10"
  TOMCAT_SERVER_PORT: "23010"
#JAVA_OPTS: -Djavax.xml.stream.XMLInputFactory=com.macsdesign.whd.util.Xml11AllowingWstxInputFactory

---
# Note that the systemPrefs needs to be persistent because that's where the
# software stores licensing information.
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: whd-frontend
  namespace: whd
spec:
  serviceName: whd-frontend
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  selector:
    matchLabels:
      app: whd-frontend
  template:
    metadata:
      labels:
        app: whd-frontend
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: cmu.ca/storage
                operator: Exists
      volumes:
      - name: apns
        secret:
          secretName: apns-cert
      - name: system-prefs-new
        persistentVolumeClaim:
          claimName: whd-system-prefs
      containers:
      - name: whd
        image: registry.gitlab.com/cmuwpg/public/webhelpdesk:12.8.3-HF2-1
        ports:
        - name: http
          containerPort: 8081
        envFrom:
        - configMapRef:
            name: whd
        - secretRef:
            name: whd
        env:
        - name: WHD_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: postgresql
              key: POSTGRES_PASSWORD
        readinessProbe:
          httpGet:
            path: /
            port: 8081
          initialDelaySeconds: 10
          periodSeconds: 30
        livenessProbe:
          exec:
            command:
              - sh
              - -c
              - 'curl -s http://localhost:8081/helpdesk/WebObjects/Helpdesk.woa | grep @@@WHD_ERROR_PAGE@@@; test $? -eq 1'
          initialDelaySeconds: 120
          periodSeconds: 60
        volumeMounts:
        - name: system-prefs-new
          mountPath: /usr/local/webhelpdesk/conf/.systemPrefs
        - name: apns
          mountPath: /usr/local/webhelpdesk/conf/apns
        resources:
          requests:
            cpu: 100m
            memory: 2Gi

---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: whd
  namespace: whd
spec:
  secretName: whd-cert
  dnsNames:
  - helpdesk.cmu.ca
  usages:
  - digital signature
  - key encipherment
  - server auth
  - client auth
  issuerRef:
    name: letsencrypt-route53
    kind: ClusterIssuer

---
apiVersion: v1
kind: Service
metadata:
  name: whd-frontend
  namespace: whd
spec:
  ports:
  - port: 80
    name: http
    targetPort: http
  selector:
    app: whd-frontend

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: whd
  namespace: whd
  annotations:
    nginx.ingress.kubernetes.io/server-snippet: |
      #        types {}
      #        default_type text/html;
      #
      #        return 200 '<html><body><h4>Maintenance</h4><p>The Web Help Desk is currently down for maintenance. It should be back up shortly.</p></body></html>';
spec:
  ingressClassName: staff
  tls:
  - secretName: whd-cert
    hosts:
    - helpdesk.cmu.ca
  rules:
  - host: helpdesk.cmu.ca
    http:
      paths:
      - backend:
          service:
            name: whd-frontend
            port:
              name: http
        path: /
        pathType: Prefix

---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: whd-frontend
  namespace: whd
spec:
  podSelector:
    matchLabels:
      app: whd-frontend
  policyTypes:
  - Ingress
  ingress:
  - from:
    - namespaceSelector:
        matchLabels:
          app.kubernetes.io/name: ingress-nginx
